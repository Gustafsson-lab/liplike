# This is an example snippet of how to run LiPLike from the command line

python ../LiPLike.py --indepdata indepentent_variable_example.csv --depdata depentent_variable_example.csv --output_fname results.csv  --has_varnames 1

less results.csv
rm results.csv
