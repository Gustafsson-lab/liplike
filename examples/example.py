"""
This is an example file to show the syntax of the LiPLile package

We use a set of random data in our example, where the regulator at position 0
uniquely fits the last 20 dependent variables

Moreover, the regulators at position 1 and 2 can independently model the 20
second-to-last dependent variables

In other words, LiPLike should only identify interactions from the 0:th
regulator to the 20 last depentdent variables
"""


import numpy as np
import matplotlib.pyplot as plt


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2019, Linköping'
__LICENSE__ = 'GNU Affero General Public License v3.0'
__CITE__ = 'doi: https://doi.org/10.1101/651596'


def get_data(nX=20, nObs=200, nY=100):
    # Simulate some random data
    assert nY > 50
    np.random.seed(0)
    X = np.random.randn(nX, nObs)
    Y = np.random.randn(nY, nObs)

    for i in range(1,21):
        Y[-i,:] = (X[0,:]*0.9) + (0.1*np.random.randn(*X[0,:].shape))
        Y[-20-i,:] = (X[1,:]*0.9 ) + (0.1*np.random.randn(*X[0,:].shape))
    X[2,:] = (X[1,:]*0.9) + (0.1*np.random.randn(*X[0,:].shape))
    return X, Y


# Now we can use lpl
import sys
sys.path.insert(0, '../')
import LiPLike as lpl
L = lpl.LiPLike()

# Example 1: A lot of data
# If the system is overdetermined (in general more obs than regulators), we
# can use LiPLike directly
X, Y = get_data()
L.fit(X, Y)


# Plotting the values in q, we see that only the upper left corner is >> than 1
# Here, we visualise the data by imshow in matplotlib, but anything that
# looks at the value q (and where q >> 1) can be used.
plt.figure()
plt.imshow(L.q)
plt.title('LiPLike results', fontsize=15)
plt.xlabel('Y', fontsize=15)
plt.ylabel('X', fontsize=15)
plt.show()

# The same if we use an l2-norm
L.fit(X, Y, penalty_term='l2')
plt.figure()
plt.title('LiPLike with L2-norm', fontsize=15)
plt.xlabel('Y', fontsize=15)
plt.ylabel('X', fontsize=15)
plt.imshow(L.q)
plt.show()

# Example 2: less data
# Now we can test to make the system underdetermined
Xfew, Yfew = get_data(nX=61, nObs=60)
#Here, the system is underdetermined and should produce an error if no other
# measures are taken!
# L.fit(Xfew, Yfew)


# Instead, we need either a penalty term
L.fit(Xfew, Yfew, penalty_term='l2')
plt.figure()
plt.title('Under-determined system with L2-norm', fontsize=15)
plt.xlabel('Y', fontsize=15)
plt.ylabel('X', fontsize=15)
plt.imshow(L.q[:20,:])
plt.show()

# Or a prior matrix
L.fit(Xfew, Yfew, use_cov=True)
plt.figure()
plt.title('Correlation prior', fontsize=15)
plt.xlabel('Y', fontsize=15)
plt.ylabel('X', fontsize=15)
plt.imshow(L.q[:20,:])
plt.show()