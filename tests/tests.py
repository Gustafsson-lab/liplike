import sys
import numpy as np
sys.path.insert(0, '..')

__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2019, Linköping'
__version__  = '0.0.1'
__LICENSE__ = 'GNU Affero General Public License V3'
__CITE__ = 'doi: https://doi.org/10.1101/651596'


import LiPLike as lpl

def get_data(nTF=20, nObs=200, nY=100):
    # First we create some random data
    np.random.seed(0)
    X = np.random.randn(nTF, nObs)
    Y = np.random.randn(nY, nObs)

    for i in range(1,21):
        Y[-i,:] = (X[0,:]*0.9) + (0.1*np.random.randn(*X[0,:].shape))
        Y[-50-i,:] = (X[1,:]*0.99) + (0.01*np.random.randn(*X[0,:].shape))
    X[2,:] = (X[1,:]*0.9) + (0.1*np.random.randn(*X[0,:].shape))
    return X, Y



def test_l2():
    X, Y = get_data()
    model = lpl.LiPLike()
    model.fit(X,Y, penalty_term='l2')
    assert (model.q[0,-1] > model.q[0,0])

def test_regression():
    X, Y = get_data()
    model = lpl.LiPLike()
    model.fit(X,Y, penalty_term='None')
    assert model.q[0,-1] > model.q[0,0]


def test_underdetermined():
    X, Y = get_data(nTF=202)
    model = lpl.LiPLike()
    try:
        CRASH = False
        model.fit(X,Y, penalty_term='None')
    except:
        CRASH = True
    assert CRASH


def test_underdetermined_w_cov():
    X, Y = get_data(nTF=51, nObs=50)
    model = lpl.LiPLike()
    model.fit(X,Y, use_cov=True)
    assert model.q[0,-1] > 10*model.q[0,0]


def test_underdetermined_w_l2():
    X, Y = get_data(nTF=160, nObs=150)
    model = lpl.LiPLike()
    model.fit(X,Y, penalty_term='l2')
    assert model.q[0,-1] > model.q[0,0]

