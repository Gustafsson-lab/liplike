#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# =============================================================================
# LiPLike
# Author: Rasmus Magnusson
# Copyright (C): Rasmus Magnusson, 2019, Linköping
# License: GNU Affero General Public License V3
#
# Please cite the original publication when using this software.
# As of now (2019-08-16), the LiPLike manuscript is available as a preprint
# at:
# https://www.biorxiv.org/content/10.1101/651596v1
# doi: https://doi.org/10.1101/651596
#
#
#
#
# The purpouse of this toolbox is to only extract idetifiable parameters in
# from linear model. Specifically, we assume a model
#
#      Y = b*X
#
# Where
#     Y holds the response varable of the genes, dim = (nObs)
#     X is a matrix with the explanatory variables, i.e. TFs. dim = (nVar, nObs)
#     b is a matrix holding the estimated parameters. dim = (nVar)
#
# Moreover, b is selected to minimize the following expression (least squares)
#
#     b_estim = min_b (Y - bX)^2
#
#
# =============================================================================
"""


import numpy as np
import numpy.matlib
from src import regression_fun as regfun
from src import parse_fun

__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2019, Linköping'
__version__ = '0.1.9'
__LICENSE__ = 'GNU Affero General Public License v3.0'
__CITE__ = 'doi: https://doi.org/10.1101/651596'
__all__ = ['LiPLike'] # We dont need the parsing functions


class LiPLike:
    """
    # The purpouse of this function is to set a way to examine the identifiability
    # of parameters in a linear model. Specifically, we assume a model
    #
    #      Y = b*X
    #
    # Where
    #     Y holds the response varable of the genes, dim = (nObs)
    #     X is a matrix with the explanatory variables, i.e. TFs. dim = (nVar, nObs)
    #     b is a matrix holding the estimated parameters. dim = (nVar)
    #
    # Moreover, b is selected to minimize the following expression (least squares)
    #
    #     b_estim = min_b (Y - bX)^2
    """

    # As of now, there are a lot of self.x = y kind of declarations that make
    # LiPLike unnecessarily complex codewise. Should be changed in future.

    def __init__(self, save_dir=''):
        self.save_dir = save_dir


    def __build_cov_prior(self):
        """
        This function is made to take care of the case when we have an
        underdetermined system. Then, we need to remove as many independent
        variables as needed to become overdetermined. The amount of variables
        removed is dependent on variable cov_q, which is the quota
        observations per dpendent variable.

        Moreover, if we have a mask, each row should, at least, reach the
        cov_q quota.
        """
        print('Building prior...')
        cov_mask = np.zeros((self.X.shape[0], self.Y.shape[0]))
        covs = cov_mask.copy()
        nKeep = int(np.linalg.matrix_rank(self.X)*self.cov_q)

        for i in range(self.Y.shape[0]):
            covs[:, i] = np.cov(self.Y[i, :], self.X)[0, 1:]
        covs = np.abs(covs)

        # Now, create the mask, and make sure that if we have a mask, still
        # remove the righ amount of variables
        for i in range(self.Y.shape[0]):
            cov_mask[np.argsort(covs[:, i])[-nKeep:], i] = 1

        self.mask = cov_mask.astype(bool)
        print('done')


    def __sanity_check(self):
        if not len(self.X.shape) == 2:
            raise ValueError('Variable X needs to be a 2D numpy matrix')

        if not self.X.shape[1] == self.Y.shape[1]:
            raise ValueError('Matrix expression Y-bX must be defined, X.shape[1] != Y.shape[1]')

        if self.mask is not None:
            if (self.mask.shape[0] != self.X.shape[0]) | (self.mask.shape[1] != self.Y.shape[0]):
                raise ValueError('Mask has wrong shape')

        if (self.penalty_term not in ['l2']) \
                & (self.mask is None) & (not self.use_cov) \
                & (self.X.shape[0] > np.linalg.matrix_rank(self.X)):
            raise ValueError('''Too few observations in relation to explanatory variables;
                             system is underdetermined!''')

        if (self.cov_q <= 0) | (self.cov_q >= 1):
            raise ValueError('Invalid value of cov_q, value must be on the interval ]0,1[')

        if (self.mask is not None) & self.use_cov:
            raise ValueError('As for now, use_cov and mask are not compatible')

        if (self.mask is not None) and (self.penalty_term != 'l2'):
            if np.any(np.sum(self.mask, 0) > self.nObs):
                raise ValueError('At least one equation is underdetermined, check mask!')

        if self.penalty_term not in ['None', 'l2']:
            raise ValueError('Unknown penalty term')


    def fit(self, X, Y, mask=None, use_cov=False, cov_q=0.9, penalty_term='None'):
        """
        Rank interactions in a linear system according to identifiability.

        Parameters
        ----------
        X,Y : array_like
            Matrices containing dependent variables (Y), and independent
            variables (Y)
        mask : array_like, optional
            Prior matrix there a True value on row i, column j indicates an
            interaction
        use_cov : bool, optional
            If no prior is given, calculate a prior based on correlation
        cov_q : float, optional
            If use_cov, how close must the system be from being determined
        penalty_term : ['None', 'l2'], optional
            Should the system be solved with an L2-term (which allows for
            underdetermined systems to be analysed)


        Attributes
        -------
        q : ndarray
            An array with the shape X.shape[0], Y.shape[0], ranging from ]0,1[.
            Higher values indicate more well-determined interactions



        Examples
        --------
        >>> import LiPLike as lpk
        >>>
        >>> # Create some data
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> nObs = 200
        >>> nY = 70
        >>> nTF = 20
        >>> X = np.random.randn(nTF, nObs)
        >>> Y = np.random.randn(nY, nObs)
        >>> Xunderdetermined = np.random.randn(nY + 1, nObs)
        >>>
        >>> # use LiPLike
        >>> L = lpk.LiPLike()
        >>> L.fit(X,Y)
        >>> print(L.q)
        >>>
        >>> # Use LiPLike on underdetermined system
        >>> L = lpk.LiPLike()
        >>> L.fit(X, Y, use_cov=True)
        >>> print(L.q)
        >>>
        >>> L = lpk.LiPLike()
        >>> L.fit(X, Y, penalty_term='l2')
        >>> print(L.q)
        >>>
        """
        # Do some first assignments of variables
        self.X = X
        self.Y = Y
        self.mask = mask
        self.nObs = Y.shape[1]
        self.use_cov = use_cov
        self.cov_q = cov_q
        self.penalty_term = penalty_term



        # Here, we do sanity checks to assure that the input fullfills all
        # specified requirements that are needed to make a LipLike

        # Note: we only do a check that the system is not underdetermined if
        # we have no mask. Else, there might be cases of where we have an
        # overdetermined system on individual dependent variables, i.e. rows of
        # the variable Y.

        self.__sanity_check()


        # If we don't use the prior, it is quicker to not leave the numpy package
        if (self.mask is None) and (not self.use_cov):
            self.fun = regfun.LiPLike_no_prior
            q = self.fun(X.copy(), Y.copy(), self.penalty_term)
            self.q = q
            return

        if use_cov:
            self.__build_cov_prior()



        # The background cost is the cost that the special cases should
        # later be compared to. This cost should be individual for each
        # dependent varable.

        # The individual costs are assigned to the matrix self.full_cost_ in
        # the format n*m, where n=number of dependent variables (Y.shape[0]),
        # and m = number of independent variables (X.shape[0].

        print('Calculating pre-cost...')
        self.full_cost_ = regfun.calc_precost(X, Y, self.mask)
        print('done')



        # Here, we calculate the cost of the special cases, i.e. where one of
        # the independent variables are removed when fitting a independent
        # variable. This creates a matrix of the same shape as self.full_cost_

        # Assign a matrix for the special cost
        special_cost = np.zeros((X.shape[0], Y.shape[0]))
        special_cost[:] = np.NaN

        for i in range(X.shape[0]):
            Xshort = X[(np.arange(X.shape[0]) != i), :]
            for j in range(Y.shape[0]):
                if not self.mask is None:
                    if not self.mask[i, j]:
                        continue
                    Xshort = X[(np.arange(X.shape[0]) != i), :]
                    Xshort = Xshort[self.mask[np.arange(X.shape[0]) != i][:, j]]
                special_cost[i, j] = regfun.get_cost(Y[j, :].T, Xshort)

            # If specified, save results after each TF
            # Later, this functionality should be split up into a new file
            if not self.save_dir == '':
                print('printing', i)
                with open(self.save_dir, 'a') as f:
                    for pos in range(len(special_cost[i, :])):
                        f.write(str(special_cost[i, pos]/self.full_cost_[i, pos]) + ',')
                    f.write('\n')

        # Now assign variables from the LiPLike calculations
        self.special_cost = special_cost

        self.div = self.special_cost/self.full_cost_
        self.q = self.div.copy()
        self.subtracted = self.special_cost - self.full_cost_


    def bootstrap(self, n=10000, X=None, Y=None, use_cov=False, cov_q=0.9, thresh=0.01):
        """
        This is a function that bootstraps the problem to see a threshold of
        what kind of fit discrepancies that can be expected by default. It
        sends the value of the thresh:th value of the divided and subtracted
        costs. Also, it bootstraps n times.

        To do:
            also take care of mask if there is one!
            Make mask to be possible to come in as a var
        """
        def __shufflecol(mat):
            for i in range(mat.shape[1]):
                np.random.shuffle(mat[:, i])
            return mat

        if (X is None) & (Y is None):
            use_independantly = False
            Xrand = self.X.copy()
            Yrand = self.Y.copy()
            _mask = self.mask
        else:
            use_independantly = True
            Xrand = X
            Yrand = Y

            if use_cov:
                self.X = X.copy()
                self.Y = Y.copy()
                self.cov_q = cov_q
                self.__build_cov_prior()
                _mask = self.mask
            else:
                _mask = None

        base = []
        special = []
        for _ in range(n):
            Xrand = __shufflecol(Xrand)
            Yrand = __shufflecol(Yrand)

            if _mask is None:
                base.append(regfun.get_cost(Yrand[0, :], Xrand))
                Xrand_short = Xrand[1:, :]
                special.append(regfun.get_cost(Yrand[0, :], Xrand_short))
            else:
                __shufflecol(_mask)
                base.append(regfun.get_cost(Yrand[0, :], Xrand[_mask[:, 0]]))
                Xrand_short = Xrand[1:, :]
                special.append(regfun.get_cost(Yrand[0, :], Xrand_short[_mask[1:, 0]]))

        base = np.array(base)
        special = np.array(special)
        div_b = special/base
        sub_b = special - base

        # There is a risk to get an underdetermined problem. remove these here
        div_b = div_b[(div_b >= 1)]
        sub_b = sub_b[sub_b >= 0]
        div_b_thresh = np.sort(div_b)[-int(thresh*len(div_b))]
        sub_b_thresh = np.sort(sub_b)[-int(thresh*len(sub_b))]

        self.bootstrap_level_div = div_b_thresh
        self.bootstrap_level_sub = sub_b_thresh
        self.bootstrap_dist_div = div_b
        self.bootstrap_dist_sub = sub_b

        if not use_independantly:
            if _mask is None:
                self.FDR_sign_div = (np.prod(self.div.shape)*thresh) \
                    /np.sum(self.div > self.bootstrap_level_div)

                self.FDR_sign_sub = (np.prod(self.subtracted.shape)*thresh) \
                    /np.sum(self.subtracted > self.bootstrap_level_sub)
            else:
                self.FDR_sign_div = (np.sum(self.mask)*thresh) \
                    /np.sum(self.div > self.bootstrap_level_div)
                self.FDR_sign_sub = (np.sum(self.mask)*thresh) \
                    /np.sum(self.subtracted > self.bootstrap_level_sub)


            # Retrieving the position of the high values requires som code. Here,
            # we simplify things for the users

            nX = self.X.shape[0]
            nY = self.Y.shape[0]

            Xid = np.repeat(np.arange(nX), nY)
            Yid = numpy.matlib.repmat(np.arange(nY), 1, nX)[0]
            print(Xid)
            div_tmp = self.div.copy()
            subtracted_tmp = self.subtracted.copy()

            div_tmp[np.isnan(div_tmp)] = 1
            subtracted_tmp[np.isnan(subtracted_tmp)] = 0

            self.Xindex_div = Xid[np.argsort(div_tmp.flatten())]
            self.Yindex_div = Yid[np.argsort(div_tmp.flatten())]
            self.Xindex_sub = Xid[np.argsort(subtracted_tmp.flatten())]
            self.Yindex_sub = Yid[np.argsort(subtracted_tmp.flatten())]





def __apply_LiPLike(args):
    import pandas as pd

    # Read in the data
    indep = pd.read_csv(args.indepdata[0])
    dep = pd.read_csv(args.depdata[0])
    if args.has_varnames:
        # Save the names of the variables if those are present
        names_dep = dep.iloc[:, 0].values
        names_indep = indep.iloc[:, 0].values

        # Remove them for now
        indep = indep.iloc[:, 1:]
        dep = dep.iloc[:, 1:]

    # Start LiPLike
    L = LiPLike()
    if args.Use_l2:
        L.fit(indep.values, dep.values, penalty_term='l2')
    else:
        L.fit(indep.values, dep.values)

    results = L.q
    results = pd.DataFrame(results)

    # If there were any variable names in the data, we add that back now
    if args.has_varnames:
        results.index = names_indep
        results.columns = names_dep
        results.to_csv(args.output_fname[0])
    else:
        results.to_csv(args.output_fname[0], index=False, columns=False)


if __name__ == '__main__':
    ARGS = parse_fun.__parse()
    __apply_LiPLike(ARGS)

