 LiPLike
================

About LiPLike
-------------
LiPLike is a tool to extract gene regulatory network interactions that are uniquely defined in the data. This is done by independently removing each regulator of a system, and observing the difference in the residual sum of squares (RSS). LiPLike calculates the increase in RSS as a fraction of RSS of full network. In other words, if an interaction is irreplaceable, LiPLike returns a value q > 1, whereas q ~= 1 indicates that an interaction is not uniquely defined (or unnecessary) in data.
2019-05-29

Installation
============
The package can be used under the license GNU Affero General Public License V3

Python Version Supported/Tested
-------------------------------
- Python 3.6

Dependencies
------------
- [NumPy version > 1.12.1](https://www.numpy.org/)

Optional, for use of L2 norm (one of two options in under-determined systems, recommended):
- [scikit-learn version > 0.19.2](https://scikit-learn.org/stable/)

Needed in order to run from the command line:
- [pandas version > 0.19.2](https://pandas.pydata.org/)
- argparse version > 1.1

Usage:
======
In python:
```python
# Here, X is an (n,k) matrix, and Y is an (m,k) matrix of floats.
>>> import LiPLike as lpl
>>> L = lpk.LiPLike()
>>> L.fit(X, Y)
>>> print(L.q)

# If the system is underdetermined, we can use an l2-norm
>>> L = lpk.LiPLike()
>>> L.fit(X, Y, penalty_term='l2')
>>> print(L.q)

# Or, we can use make LiPLike to calculate a prior matrix from a correlation cut-off
>>> L = lpk.LiPLike()
>>> L.fit(X, Y, use_cov=True)
>>> print(L.q)
```
Or from the command line:
```console
python LiPLike.py --indepdata indepentent_variable_example.csv --depdata depentent_variable_example.csv
```

In depth description of LiPLike
===============================
LiPLike is a novel gene regulatory network (GRN) inference method to maximise the accuracy of gene regulatory predictions. LiPLike minimises the false positive prediction rate by not identifying potential regulators where a regulation can be replaced by a linear combination of one or several other explanatory variables (Fig. 1a). Specifically, the LiPLike algorithm was inspired by the profile likelihood method, used to estimate confidence intervals of estimated model parameters. Consider a system of independent variables $`X`$ and dependent variables $`Y`$.

```math
Y_{j,k} = \sum_i\beta_{i,j}  X_{i,k}+ \varepsilon
```

In the annotations of the equation above, $`Y_{j,k}`$ is a scalar corresponding to the expression of gene _j_ at observation _k_. Likewise, $`X_{i,k}`$ is a scalar corresponding to regulator _i_ at observation _k_. As is widely known, for overdetermined systems the vector $`\beta`$ can be analytically estimated to minimise the sum of squared residuals. Now, for each parameter $`\beta_{i,j}`$, there are two parameter values that are of interest, namely the one minimising the RSS and the point where $`\beta_{i,j} = 0`$. Thus, we can quantify the relationship between these two points and introduce the term $`q_{i,j}`$.

```math
 q_{i,j} = \frac{\min_{(\beta\vert\beta_{i,j} = 0)} \sum_k(Y_{j,k}-\sum_m\beta_{m,j}X_{m,k})^2 }{\min_{\beta}\sum_k(Y_{j,k}-\sum_m\beta_{m,j}X_{m,k})^2}
```

Given the values of $`Y`$ and $`X`$, LiPLike returns $`q_{i,j}`$ for all interactions in the network. As written above, $`q_{i,j}`$ will take larger values for edges that uniquely explain $`Y_j`$. In the case of two highly correlated regulators, $`X_A`$ & $`X_B`$, the constraint $`\beta_{a,j}= 0`$ will not independently give a considerable increase in the RSS. Thus, $`q_{a,j} \approx 1`$, as $`\beta_{b,j}`$ will adjust accordingly.

In the case on an under-determined system, $`q_{i,j}`$ is undefined (as the denominator equals zero). Therefore, LiPLike can either create a prior matrix from a correlation network, or by incorporating _L2_ norms to the LiPLike equation.

```math
 q_{i,j} = \frac{\min_{(\beta\vert\beta_{i,j} = 0)} \sum_k(Y_{j,k}-\sum_m\beta_{m,j}X_{m,k})^2 + \lambda_{CV} \sum_m \beta_{m,j} }{\min_{\beta}\sum_k(Y_{j,k}-\sum_m\beta_{m,j}X_{m,k})^2 + \lambda_{CV} \sum_m \beta_{m,j} }
```

Where $`\lambda_{CV}`$ by default is chosen to minimize the leave-one-out cross validation error of a ridge regression problem. As of now, the construction of a prior matrix from correlation, LiPLike is considerably slower than using an _L2_ norm, due do the calculation of _q_ with an _L2_ norm carrying less overhead between calls to NumPy.


Contributor:
=============

 Rasmus Magnusson: Development of the package.

Current Members in the Project
------------------------------
- @rasma87

References & how to cite
======================
- [Rasmus Magnusson, Mika Gustafsson, LiPLike: towards gene regulatory network predictions of high certainty, Bioinformatics, , btz950, https://doi.org/10.1093/bioinformatics/btz950](https://www.ncbi.nlm.nih.gov/pubmed/31904818)
