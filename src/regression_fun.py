#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Here we store all the functions related to regression that LiPLike can call
"""


import numpy as np


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2019, Linköping'
__version__ = '0.0.1'
__LICENSE__ = 'GNU Affero General Public License v3.0'
__CITE__ = 'doi: https://doi.org/10.1101/651596'
__all__ = ['get_cost', 'calc_precost', 'l2_model_fit', 'regression_model_fit', 'LiPLike_no_prior']


def get_cost(Y, X):
    """
    Add explanation here
    """
    beta = np.linalg.lstsq(X.T, Y)[0] # The cost is returned at place 1
    cost = np.sum((Y - np.dot(X.T, beta))**2, axis=0)
    return cost


def calc_precost(X, Y, mask):
    """
    Add explanation here
    """
    if mask is None:
        full_cost = np.matlib.repmat(get_cost(Y.T, X), X.shape[0], 1)
    else:
        cost = []
        for i in range(Y.shape[0]):
            beta = np.linalg.lstsq(X[mask[:, i], :].T, Y[i, :].T)[0]
            cost.append(np.sum((Y[i, :].reshape(len(Y[i, :]), 1) - np.dot(X[mask[:, i]].T, \
                                beta.reshape((len(beta), 1))))**2, axis=0))

        full_cost = np.matlib.repmat(np.array(cost).T, X.shape[0], 1)
    return full_cost

def l2_model_fit(X, Y, **kwargs): # Dont worry bout kwargs,for convention
    # Check if we can import ridge from sklearn
    try:
        import sklearn.linear_model as lm
    except:
        raise ValueError('sklearn not installed, penalty_term l2 not available')
    alphas = np.array([10**i for i in range(-4, 4)])
    l2 = lm.RidgeCV(alphas=alphas)
    l2.fit(X.T, Y.T)
    return (Y - np.matmul(l2.coef_, X))**2


def regression_model_fit(X, Y, **kwargs): # Dont worry bout kwargs,for convention
    beta = np.linalg.lstsq(X.T, Y.T)[0] # The cost is returned at place 1
    return ((Y.T - np.matmul(X.T, beta))**2).T


def LiPLike_no_prior(X, Y, penalty_term='None'):
    if penalty_term == 'None':
        eval_fun = regression_model_fit
    elif penalty_term == 'l2':
        eval_fun = l2_model_fit

    full_cost = np.sum(eval_fun(X, Y), 1)

    # Now calculate the short cost
    short_cost = []
    nExplan = X.shape[0]
    for i in range(nExplan):
        Xshort = X[np.arange(nExplan) != i, :]
        short_cost.append(np.sum(eval_fun(Xshort, Y), 1))
    q = np.array([x/full_cost for x in short_cost])
    return q

