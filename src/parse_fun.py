#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Here we store all the functions related to argument parsing that LiPLike can call
"""


import numpy as np


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2019, Linköping'
__version__ = '0.0.1'
__LICENSE__ = 'GNU Affero General Public License v3.0'
__CITE__ = 'doi: https://doi.org/10.1101/651596'
__all__ = ['__parse',]


def __parse(): #Should I move this to a UTIL folder?
    import argparse
    DESC = '''LiPLike is a tool to extract gene regulatory network interactions
    that are uniquely defined in the data. This is done by independently
    removing each regulator of a system, and observing the difference in the
    residual sum of squares (RSS). LiPLike calculates the increase in RSS as
    a fraction of RSS of full network. In other words, if an interaction is
    irreplaceable, LiPLike returns a value q > 1, whereas q ~= 1 indicates
    that an interaction is not uniquely defined (or unnecessary) in data.

    Dependancies:
        - Python3.6
        - NumPy 1.12.1
        - scikit-learn 0.19.2

    If LiPLike is called as an executable:
        - argparse 1.1
        - pandas 0.19.2

    Author:
        - Rasmus Magnusson

    COPYRIGHT:
        - Rasmus Magnusson Linköping 2019

    LICENCE:
        - - GNU Affero General Public License v3.0

    Further reference:
        - https://gitlab.com/Gustafsson-lab/liplike
        - https://www.biorxiv.org/content/10.1101/651596v1
        - doi: https://doi.org/10.1101/651596
    '''
    # Create the parser instance
    parser = argparse.ArgumentParser(description=DESC)
    parser.add_argument('--indepdata',
                        type=str,
                        nargs=1,
                        help='Name of the file containing the independent variable data (X)')
    parser.add_argument('--depdata',
                        type=str,
                        nargs=1,
                        help='Name of the file containing the dependent variable data (Y)')
    parser.add_argument('--output_fname',
                        default='results.csv',
                        type=str,
                        nargs=1,
                        help='Name of the output file')
    parser.add_argument('--header',
                        type=int,
                        default=0,
                        nargs=1,
                        help='Pass 1 if the files contain a header')
    parser.add_argument('--has_varnames',
                        type=int,
                        default=0,
                        nargs=1,
                        help='Pass 1 if the files contain a first column with variable names')
    parser.add_argument('--Use_l2',
                        type=int,
                        default=0,
                        nargs=1,
                        help='Pass 1 if LiPLike should apply a L2 penalty')
    args = parser.parse_args()

    return args